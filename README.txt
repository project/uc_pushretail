PushRetail provides you a free mobile app which allows you to manage your e-commerce site with phone or tablet. You could check orders, receive notifications, view customers and products info. We are always improving and adding new app features.

First beta release include push notifications, orders, customers view and stock levels and prices list.
 The main idea of an app to allow you access all necessary info about your commerce site from mobile device.

Pushretail For Ubercart Installation instructios:
1. Enable Pushretail For Ubercart (uc_pushretail) module and it's dependencies: services, uc_store, uc_order, uc_product, rest_server (Ubercart and Services modules).
2. Not required. Register an account on http://pushretail.com/ and get an API key. Go to http://yoursite.com/admin/config/services/pushretail and enter a Key.
3. Go to /admin/structure/services to be sure "pushretail" service endpoint exists and installed correctly.
4. Instal an Android app on your phone or tablet and login to your site. Here it is!

Feel free to post a bugs and ask for a new features!